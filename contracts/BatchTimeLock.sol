pragma solidity 0.4.25;

import "../node_modules/openzeppelin-solidity/contracts/ownership/Ownable.sol";
import "../node_modules/openzeppelin-solidity/contracts/token/ERC20/ERC20Basic.sol";

/**
 * @title BatchTimeLock
 * @dev BatchTimeLock is a token holder contract that will distribute tokens
 * to multiple investors after a time lock periode
 */
contract BatchTimeLock is Ownable {
    // ERC20 basic token contract being held
    ERC20Basic public token;
    // beneficiary of tokens after they are released
    address[] public investors;
    // quantity of tokens each investor will receive 
    uint256[] public amounts;
    // timestamp when token release is enabled
    uint256 public releaseTime;
    // registerInvestors should be callable only once 
    bool public registered;

    constructor(ERC20Basic _token, uint256 _releaseTime) public {
        require(_releaseTime > block.timestamp);
        token = _token;
        releaseTime = _releaseTime;
        registered = false;
    }

    /**
     * @notice Transfers tokens held by timelock to all investors.
     */
    function batchTransfer() external {
        require(registered);
        require(block.timestamp >= releaseTime);
        for (uint i=0; i<investors.length; i++) {
            require(token.transfer(investors[i], amounts[i]));
        }
    } 

    /**
     * @notice register investors and the balance they will receive after timelock
     * Can only be called once after that the tokens are locked for investors.
     */
    function registerInvestors(address[] _investors, uint256[] _amounts) onlyOwner external {
        require(!registered);
        registered = true;
        uint total = _investors.length;
        assert(_amounts.length == total); // just in case
        investors = _investors;
        amounts = _amounts;
    }


    /**
     * @notice read investor's balance
     */
    function getBalance(address _investor) external view returns (uint256) {
        // getter, not to be executed as a transaction (expensive)
        for (uint i=0; i<investors.length; i++) {
            if (investors[i] == _investor) {
                return amounts[i];
            }
        }
        return 0;
    }
}
