pragma solidity 0.4.25;

import "../node_modules/openzeppelin-solidity/contracts/ownership/Ownable.sol";
import "../node_modules/openzeppelin-solidity/contracts/token/ERC20/ERC20Basic.sol";

/**
 * @title BatchTokenPayment
 * @dev This contract is a script that transfers AergoToken to investors. 
 *      Called once a month to transfer in batch.
        First send tokens to this contract and then call batchTransfer.
 */
contract BatchTokenPayment is Ownable {
    ERC20Basic public token;

    constructor(ERC20Basic _token) public {
        token = _token;
    }

    function batchTransfer(address[] addrs, uint256[] amounts) onlyOwner external {
        uint total = addrs.length;
        assert(amounts.length == total); // just in case
        for (uint i=0; i<total; i++) {
            assert(token.transfer(addrs[i], amounts[i]));
        }
    } 
}
