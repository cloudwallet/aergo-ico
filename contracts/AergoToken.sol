pragma solidity 0.4.25;

import "../node_modules/openzeppelin-solidity/contracts/token/ERC20/StandardToken.sol";
import "../node_modules/openzeppelin-solidity/contracts/ownership/Ownable.sol";

/**
 * @title AergoToken
 * @dev Very simple ERC20 Token that is initialised with presale balances.
 *      This contract does not prevent sending tokens to an account that does not handle ERC20 tokens.
 */
contract AergoToken is StandardToken, Ownable {
    
    string public constant name = "Aergo Crowdsale Token";
    string public constant symbol = "AERGO";
    uint8 public constant decimals = 18;
    bool public paused = true;

    event Pause();
    event Unpause();


    /**
     * Modifiers
     */

    /**
    * @dev Modifier to make a function callable only when the contract is not paused.
    */
    modifier whenNotPaused() {
        require(!paused || (msg.sender == owner));
        _;
    }

    /**
    * @dev called by the owner to pause, triggers stopped state
    */
    function pause() onlyOwner public {
        paused = true;
        emit Pause();
    }

    /**
    * @dev called by the owner to unpause, returns to normal state
    */
    function unpause() onlyOwner public {
        paused = false;
        emit Unpause();
    }

    /**
     * Constructor
     */
    constructor() public {
        // totalSupply_ = sum of all the tokens created in this constructor
        totalSupply_ = 500 * (10**6) * 10**18;
        
        // issue 500 million tokens to ledger1
        //balances[0x0E00Ca66f8717428bd0588425296313516785A29] = 500 * (10**6) * 10**18;
        // This balance is for truffle test
        balances[msg.sender] = 1000 * (10**18);
    }

    /**
     * Functions overridden so that when the contract is paused, they are not callable by anyone except the owner.
     */

    function transfer(address _to, uint256 _value) public whenNotPaused returns (bool) {
        return super.transfer(_to, _value);
    }

    function transferFrom(address _from, address _to, uint256 _value) public whenNotPaused returns (bool) {
        return super.transferFrom(_from, _to, _value);
    }

    function approve(address _spender, uint256 _value) public whenNotPaused returns (bool) {
        return super.approve(_spender, _value);
    }

    function increaseApproval(address _spender, uint _addedValue) public whenNotPaused returns (bool success) {
        return super.increaseApproval(_spender, _addedValue);
    }

    function decreaseApproval(address _spender, uint _subtractedValue) public whenNotPaused returns (bool success) {
        return super.decreaseApproval(_spender, _subtractedValue);
    }
}
