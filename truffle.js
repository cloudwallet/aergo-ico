module.exports = {
  // See <http://truffleframework.com/docs/advanced/configuration>
  // to customize your Truffle configuration!

	networks: {
		ganache: {
			host: "localhost",
			port: 8545,
			network_id: 5777,
			//gasPrice: 30e9,
			//gas: 6000000
		},
		live: {
			host: "", // Random IP for example purposes (do not use)
			port: 80,
			network_id: 1,        // Ethereum public network
			gasPrice: 8000000000
			// optional config values:
			// gas
			// gasPrice
			// from - default address to use for any transaction Truffle makes during migrations
			// provider - web3 provider instance Truffle should use to talk to the Ethereum network.
			//          - function that returns a web3 provider instance (see below.)
			//          - if specified, host and port are ignored.
			}
		
	}
};
