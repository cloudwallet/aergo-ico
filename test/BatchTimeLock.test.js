const AergoToken = artifacts.require('AergoToken')
const BatchTimeLock = artifacts.require('BatchTimeLock')
const { latestTime } = require('../node_modules/openzeppelin-solidity/test/helpers/latestTime');
const { increaseTimeTo, duration } = require('../node_modules/openzeppelin-solidity/test/helpers/increaseTime');
const { expectThrow } = require('../node_modules/openzeppelin-solidity/test/helpers/expectThrow');
investors = require('./investors');

const BigNumber = web3.BigNumber;

require('chai')
  .use(require('chai-bignumber')(BigNumber))
  .should();

contract('BatchTimeLock', function([payer, receiver, owner]) {
    describe('Transfer tokens in batch to investors after timelock', async function () {
        it('create AergoToken and BatchTimeLock', async function () {
            this.token = await AergoToken.new( { from: payer } );

            let balance = await this.token.balanceOf(payer);
            balance.should.be.bignumber.equal(new BigNumber(1000*10**18));

            balance = await this.token.balanceOf(receiver);
            balance.should.be.bignumber.equal(new BigNumber(0));

            this.releaseTime = (await latestTime()) + duration.years(1);
            this.BTL= await BatchTimeLock.new(this.token.address, this.releaseTime, { from: owner });
        });
        it('unpause AergoToken', async function () {
            await this.token.unpause({ from: payer });
            assert.equal(await this.token.paused(), false);
        });
        it('transfer tokens to BatchTimeLock', async function () {
            await this.token.transfer(this.BTL.address, new BigNumber(1000*10**18), { from: payer });

            let balance = await this.token.balanceOf(this.BTL.address);
            balance.should.be.bignumber.equal(new BigNumber(1000*10**18));

            balance = await this.token.balanceOf(payer);
            balance.should.be.bignumber.equal(new BigNumber(0));

            balance = await this.token.balanceOf(receiver);
            balance.should.be.bignumber.equal(new BigNumber(0));
        });
        it('register investors', async function() {
            investors.array[99] = receiver;
            let amount = new BigNumber(10*10**18)
            let txInfo = await this.BTL.registerInvestors(investors.array, new Array(100).fill(amount), { from: owner });
            console.log("Gas for registering investors", txInfo.receipt.gasUsed);
        });
        it('chech timelocked balance', async function() {
            let balance = await this.BTL.getBalance.call(receiver)
            balance.should.be.bignumber.equal(new BigNumber(10*10**18));

            addr0 = "0x0000000000000000000000000000000000000000",
            balance = await this.BTL.getBalance.call(addr0)
            balance.should.be.bignumber.equal(new BigNumber(0));
        });
        it('cannot batch transfer before release time', async function() {
            await expectThrow(this.BTL.batchTransfer());
        });
        it('batch transfer after release time', async function() {
            await increaseTimeTo(this.releaseTime);
            let txInfo = await this.BTL.batchTransfer();
            console.log("Gas used for transfering tokens in batch", txInfo.receipt.gasUsed);

            let balance = await this.token.balanceOf(this.BTL.address)
            balance.should.be.bignumber.equal(new BigNumber(0));

            balance = await this.token.balanceOf(receiver)
            balance.should.be.bignumber.equal(new BigNumber(10*10**18));
        });
    });
});
