const AergoToken = artifacts.require('AergoToken')
const BatchTokenPayment = artifacts.require('BatchTokenPayment')
investors = require('./investors');

const BigNumber = web3.BigNumber;

require('chai')
  .use(require('chai-bignumber')(BigNumber))
  .should();


contract('BatchTokenPayment', function([payer, receiver, owner]) {
    describe('Transfer tokens in batch to investors', async function () {
        it('create AergoToken and BatchTokenPayment', async function () {
            this.token = await AergoToken.new( { from: payer } );

            let balance = await this.token.balanceOf(payer);
            balance.should.be.bignumber.equal(new BigNumber(1000*10**18));

            balance = await this.token.balanceOf(receiver);
            balance.should.be.bignumber.equal(new BigNumber(0));
            this.BTP = await BatchTokenPayment.new(this.token.address, { from: owner });
        });
        it('unpause AergoToken', async function () {
            await this.token.unpause({ from: payer });
            assert.equal(await this.token.paused(), false);
        });
        it('transfer tokens to BatchTokenPayment', async function () {
            await this.token.transfer(this.BTP.address, new BigNumber(1000*10**18), { from: payer });

            let balance = await this.token.balanceOf(this.BTP.address);
            balance.should.be.bignumber.equal(new BigNumber(1000*10**18));

            balance = await this.token.balanceOf(payer);
            balance.should.be.bignumber.equal(new BigNumber(0));

            balance = await this.token.balanceOf(receiver);
            balance.should.be.bignumber.equal(new BigNumber(0));
        });
        it('transfer tokens in batch to investors', async function() {
            investors.array[99] = receiver;
            let amount = new BigNumber(5*10**18)
            let txInfo = await this.BTP.batchTransfer(investors.array, new Array(100).fill(amount), { from: owner });
            console.log("Gas for transfering tokens in batch to new addresses", txInfo.receipt.gasUsed);
            let txInfo2 = await this.BTP.batchTransfer(investors.array, new Array(100).fill(amount), { from: owner });
            console.log("Gas used for transfering tokens in batch", txInfo2.receipt.gasUsed);

            let balance = await this.token.balanceOf(this.BTP.address)
            balance.should.be.bignumber.equal(new BigNumber(0));

            balance = await this.token.balanceOf(receiver)
            balance.should.be.bignumber.equal(new BigNumber(10*10**18));
        });
    });
});


