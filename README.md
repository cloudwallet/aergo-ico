
# Aergo ICO Smart Contracts

## Quick Start

```sh
npm install
``` 

Deploy contracts using remix.


## Environment set up

### Nodejs

```sh
brew install node
```

### Truffle
```sh
npm install -g truffle
```
http://truffleframework.com/docs/getting_started/client

### Ethereum development client

Ganache is the new testrpc.
Install Ganache CLI with :
```sh
npm install -g ganache-cli
```
or download the GUI at http://truffleframework.com/ganache/

## Smart contract libraries
Install using package.json :
```sh
npm install
``` 

We will use the zeppelin-solidity library as it is commonly used in ICOs and well documented
https://github.com/OpenZeppelin/zeppelin-solidity  
  
Create package from scratch :  
```sh
npm init -y
npm install -E zeppelin-solidity
```

## Tests
```sh
truffle test
```

## Debug with Remix
### Install remixd
```sh
npm -g install remixd@0.1.5 // this version works with http://remix-alpha.ethereum.org
npm -g install remixd // with remix-app https://github.com/horizon-games/remix-app/releases
remixd -s /absolute/path/to/argo-ico
```

Access remix IDE at http://remix-alpha.ethereum.org

